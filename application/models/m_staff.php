<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_staff extends CI_Model
/**********************************************************************************************************************************
*
**********************************************************************************************************************************/
  {
       function __construct()
  {
       // Call the Model constructor
       parent::__construct();
  }
       //get the username & password from tbl_usrs
       function get_user()
  {
       $this->db->select('*');
       $this->db->from('staff');
       $query = $this->db->get();

       if($query->num_rows() > 0){
       return $query->result();
       }      
  }
/**********************************************************************************************************************************
*
**********************************************************************************************************************************/
         function get_userby_id($iduser)
    {
         $this->db->select('*');
         $this->db->from('login u');
         $this->db->join('staff k', 'k.staff_id = u.staff_id');
         $this->db->where('u.staff_id', $iduser); 

         $query = $this->db->get();

         if($query->num_rows() > 0){
         return $query->result();
         }       
    }
/**********************************************************************************************************************************
*
**********************************************************************************************************************************/
         function add_staff_data($data)
    {
         return $this->db->insert('cuti', $data);
    }
/**********************************************************************************************************************************
*
**********************************************************************************************************************************/
         function get_datacutiby_id($iduser)
    {
         $this->db->select('*');
         $this->db->from('login u');
        $this->db->join('staff k', 'k.staff_id = u.staff_id');
         $this->db->join('cuti d','d.staff_id = k.staff_id');
         $this->db->join('jenis_cuti c','c.jen_cuti = d.jns_cuti');

         $this->db->where('u.staff_id', $iduser); 

         $query = $this->db->get();

         if($query->num_rows() > 0){

         return $query->result();
       } 
    }


    function updateCuti($idCuti, $data)
    {
        $this->db->where('cuti_id', $idCuti);
        $this->db->update('cuti', $data);
    }
/**********************************************************************************************************************************
*
**********************************************************************************************************************************/
    
  }
