<?php
class PostModel extends CI_Model {
 
function getPosts($staffid){
  $this->db->select("l.username,s.*");
  $this->db->from('staff s');
  $this->db->join('login l','l.staff_id= s.staff_id');
  $this->db->where('s.staff_id',$staffid);
  $query = $this->db->get();
  return $query->result();
}


 function getData(){
  $this->db->select("*");
  $this->db->from('cuti');
  $query = $this->db->get();
  return $query->result();
}


function update($userid, $data){
  $this->db->where('staff_id',$userid);
  $this->db->update('staff',$data);

}

function delete($userid, $data){
  $this->db->where('staff_id',$userid);
  $this->db->delete('staff',$data);

}

 function getStaff($data){
  $this->db->select("*");
  $this->db->from('staff ');
  $query = $this->db->get();
  return $query->result();
}

} ?>

