<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class login extends CI_Controller{
/**********************************************************************************************************************************
* Author            : Muhammad Akram Bin Md Fadilah
* Date              : 14 July 2015
* Function List     : function __construct(), ..
**********************************************************************************************************************************/
         public function __construct()
    {
         parent::__construct();
         //load the login model
         $this->load->model('m_login');
    }
/**********************************************************************************************************************************
*End of function __construct(), ..
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* Author            : Muhammad Akram Bin Md Fadilah
* Date              : 14 July 2015
* Function List     : function index(), ..
**********************************************************************************************************************************/

           public function index()
    {
         //get the posted values
         $username = $this->input->post("Username1");
         $password = $this->input->post("Password1");

         //set validations
         $this->form_validation->set_rules('Username1', 'tidak ada usernama', 'required');
         $this->form_validation->set_rules('Password1', 'mana password', 'required');

         if ($this->form_validation->run() == FALSE)
    {    //validation fails
         //echo validation_errors(); 
         $this->load->view('staff/v_login');
    }
         else
    {    //validation succeeds
         if ($this->input->post('btn_login') == "Login")
    {
         //check if username and password is correct
         $usr_result = $this->m_login->get_user($username, $password);
         if ($usr_result > 0) //active user record is present
    {    //set the session variables
         $sessiondata = array(
        'username' => $username,
        'loginuser' => TRUE,
        'staff_id'=>$usr_result[0]->staff_id, 
        'role'=>$usr_result[0]->r_id,);
         $this->session->set_userdata($sessiondata);
         redirect("staff/profil");

         // print_r( $sessiondata);
         // die('1');
    }
         else
    {
         $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
         redirect('login/index');
         // die('2');
    }
    }
    else
    {    //  redirect('login/index');
         // die('3');
               }
          }
     }
/**********************************************************************************************************************************
*End of function index
**********************************************************************************************************************************/
}?>
