<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {



	   public function __construct()
     {
          parent::__construct();
       
        $this->load->model('m_login');
        $this->load->model('m_staff');
        $this->load->model('PostModel');
      }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

    public function index()
    {
        $data='';


        $this->load->view('template/v_login',$data);
        }

	public function login()
	{
        $data='';


		$this->load->view('template/v_login',$data);
		}

	public function pengurusan()
	{  

        $staffid=$this->session->userdata('staff_id'); 
        $d['posts']=$this->PostModel->getData($staffid);

        

        $d['role']=$this->session->userdata('role');
        $data['content']=$this->load->view('admin/v_pengurusan_cuti',$d,true);
		$this->load->view('template/dashboard',$data);

        $nama=  $this->input->post("nama");

   
	}

    function approveCuti($idCuti)
    {
        $staffid=$this->session->userdata('staff_id');

        //$idCuti = $this->input->post('idCuti');

        //print_r($idCuti);
        //die();

        $datac = array(
            "kelulusan" => "lulus"


        );

        $this->m_staff->updateCuti($idCuti, $datac);

        $d['posts']=$this->PostModel->getData($staffid);
        $d['role']=$this->session->userdata('role');
        $data['content']=$this->load->view('admin/v_pengurusan_cuti',$d,true);
        $this->load->view('template/dashboard',$data);
    }

    public function profil() {

         $staffid=$this->session->userdata('staff_id');
         $d['staff_id']=$this->m_staff->get_userby_id($staffid);
         
         $d['role']=$this->session->userdata('role');

         $d['title']='test';
         $data['content']=$this->load->view('staff/v_profil.php',$d,true);
         $this->load->view('template/dashboard',$data);

    }

	public function tolak()
	{
        $userid=$this->session->userdata('userid');
        $d['posts']= $this->PostModel->getData($userid);
        $data['content']=$this->load->view('admin/xlulus',$d,true);
		$this->load->view('template/dashboard',$data);

	}


        public function info()
    {
        $d['title']='test';
        $data['content']=$this->load->view('admin/v_infoadmin',$d,true);
        $this->load->view('template/dashboard',$data);


    }


    //index function
    public function tambah()
    {
        
        //print_r($_POST);
        //die();

        $d['role']=$this->session->userdata('role');
        $data['content']=$this->load->view('admin/v_addstaff',$d,true);
        $this->load->view('template/dashboard',$data);
        

        //$username=  $this->input->post("Nama");
        //$password=  $this->input->post("password");
        //$email=  $this->input->post("email");
        //$no_id=  $this->input->post("no_id");
        //$status=  $this->input->post("status");


        //set validation rules
        $this->form_validation->set_rules('Username', 'Username', 'trim|required|xss_clean|callback_alpha_only_space');
        //$this->form_validation->set_rules('Password', 'Password', 'required');
        //$this->form_validation->set_rules('Email', 'Email', 'trim|required|xss_clean|callback_alpha_only_space');
        //$this->form_validation->set_rules('Status', 'Status', 'trim|required|xss_clean|callback_alpha_only_space');
        //$this->form_validation->set_rules('No_id', 'No_id', 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            //echo "daniel";
            //die('1');
            //echo validation_errors();
            //fail validation
         // $this->load->view('admin/v_addstaff', $data);
        }
        else
        {    
            //echo "AFIQ";
            //die('2');
            //pass validation
            $data = array(
                'nama' => $this->input->post('Nama'),
                'alamat' => $this->input->post('Alamat'),
                'email' => $this->input->post('Email'),
                //'status' => $this->input->post('Status'),
                'no_tel' => $this->input->post('No_Tel'),
            );

            //insert the form data into database
            $this->db->insert('staff', $data);
            $staff_id = $this->db->insert_id();



            $data2 = array(
                'username' => $this->input->post('Username'),
                'password' => $this->input->post('Password'),
                'r_id' => $this->input->post('Role_id'),
                'staff_id' =>  $staff_id
                
            );

            $this->db->insert('login', $data2);


            //display success message
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Employee details added to Database!!!</div>');
            redirect('admin/semak');
        }

    }

    //custom validation function to accept only alpha and space input
    function alpha_only_space($str)
    {
        if (!preg_match("/^([-a-z ])+$/i", $str))
        {
            $this->form_validation->set_message('alpha_only_space', 'The %s field must contain only alphabets or spaces');
            return TRUE;
        }
        else
        {
            return TRUE;
        }
    }

    public function update()
    {   
   

        $staffid=$this->session->userdata('staff_id');
        // print_r($userid); 
        // die();
        $d['role']=$this->session->userdata('role');
        $d['posts'] = $this->PostModel->getPosts($staffid);
        $data['content']=$this->load->view('admin/v_update',$d,true);
        $addData =  $this->load->view('template/dashboard',$data);
    }

    public function updatedata(){
        // print_r($_POST);
        // die();

        // $userid=$this->session->userdata('userid');
        $data = array(
                'nama' => $this->input->post('nama'),
                'email' => $this->input->post('email'),
                //'id' => $this->input->post('id'),
                'alamat' => $this->input->post('address'),
                'no_tel' => $this->input->post('numtel'),
            );

        $this->db->where(array('staff_id'=> $this->input->post('staff_id')));
    $update = $this->db->update('staff', $data);

     $update = array(
               'username'=> $this->input->post('username'),
            );

    $this->db->where(array('staff_id'=> $this->input->post('staff_id')));
    $update = $this->db->update('login', $update);





    //$updte=$this->input->post('id');
    // print_r($updte);
    // // die();
   // $update1=$this->PostModel->update($updte, $data);
    if($update == '1')

        {   
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Users detail have been save to Database!!!</div>');
            redirect('admin/update');
        }
    }

}
?>