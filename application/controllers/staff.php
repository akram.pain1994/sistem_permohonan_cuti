<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class staff extends CI_Controller {



/**********************************************************************************************************************************
* Description 		: load model('m_login');,model('m_staff');
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : function __construct(), ..
**********************************************************************************************************************************/
	     public function __construct()
    {      
         parent::__construct();

         $this->load->model('m_login');
         $this->load->model('m_staff');
          $this->load->model('m_addstaffadmin');

    }    
/**********************************************************************************************************************************
*End of function __construct
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* Description 		: load v_login.php
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : function index(), ..
**********************************************************************************************************************************/
         public function index()
	{
         $data='';
		 $this->load->view('template/v_login',$data);
	}
/**********************************************************************************************************************************
*End of function index
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* File Name         : v_semakan.php
* Description 		: adds the staff information into staff table
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : function semakan_permohonan(),get_datacutiby_id(), ..
**********************************************************************************************************************************/
	     public function semakan_permohonan()
	{
         $staffid=$this->session->userdata('staff_id');
         $d['semakan']=$this->m_staff->get_datacutiby_id($staffid);
         $d['role']=$this->session->userdata('role');


         $d['title']='test';
         $data['content']=$this->load->view('staff/v_semakan.php',$d,true);
		 $this->load->view('template/dashboard',$data);
	}
/**********************************************************************************************************************************
*End of v_semakan.php
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* File Name         : v_permohonan.php
* Description 		: add data permohonan into database
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : permohonan(),get_userby_id(), ..
**********************************************************************************************************************************/
		 public function permohonan()
	{
         $staffid=$this->session->userdata('staff_id');
         $d['staff_id']=$this->m_staff->get_userby_id($staffid);
         $d['role']=$this->session->userdata('role');


         $d['title']='test';
            $d['jenis_cuti']=$this->m_addstaffadmin->getJenisCuti();
         $data['content']=$this->load->view('staff/v_permohonan.php',$d,true);
		 $this->load->view('template/dashboard',$data);
    }
/**********************************************************************************************************************************
*End of v_permohonan.php
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* File Name         : v_cpermohonan.php
* Description 		: get data from database for v_cpermohonan.php
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : c_permohonan(),get_userby_id(), ..
**********************************************************************************************************************************/		
	     public function c_permohonan()
	{    
		 $staffid=$this->session->userdata('staff_id');
         $d['cdata']=$this->m_staff->get_cdataby_id($staffid);
         $d['role']=$this->session->userdata('role');

      
         $d['title']='test';
         $data['content']=$this->load->view('staff/v_cpermohonan.php',$d,true);
		 $this->load->view('template/dashboard',$data);
	}
/**********************************************************************************************************************************
*End of v_cpermohonan.php
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* File Name         : v_profil.phsp
* Description 		: get data from database for v_profil.php
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : profil(),..
**********************************************************************************************************************************/
		 public function profil()
	{
         $staffid=$this->session->userdata('staff_id');
         $d['staff_id']=$this->m_staff->get_userby_id($staffid);
         
         $d['role']=$this->session->userdata('role');

         $d['title']='test';
         $data['content']=$this->load->view('staff/v_profil.php',$d,true);
		 $this->load->view('template/dashboard',$data);
	   	}
/**********************************************************************************************************************************
*End of v_profil.php
**********************************************************************************************************************************/

/**********************************************************************************************************************************
* File Name         : v_info.php
* Description 		: load v_info.php
* Author            : Muhammad Akram Bin Md Fadilah
* Date 				: 14 July 2015
* Function List     : profil(),..
**********************************************************************************************************************************/

		 public function info()
	{      
        $d['role']=$this->session->userdata('role');

        $d['title']='test';
        $data['content']=$this->load->view('staff/v_info',$d,true);
        $this->load->view('template/dashboard',$data);
    }
/**********************************************************************************************************************************
*End of v_info.php
**********************************************************************************************************************************/
/**********************************************************************************************************************************
* File Name         : add_cuti
* Description       : 
* Author            : Muhammad Akram Bin Md Fadilah
* Date              : 14 July 2015
* Function List     : profil(),..
**********************************************************************************************************************************/
 public function add_cuti(){

    //print_r($_POST);
    //die();

         $tarikhmula=  $this->input->post("tarikhmula");
         $tarikhakhir=  $this->input->post("tarikhakhir");
         $sebab_cuti=  $this->input->post("sebab_cuti");
         $staff_id=  $this->input->post("StaffID");
         $jns_cuti= $this->input->post("Jenis_Cuti");
         

         if($staff_id){
         $datacuti= array(

        
        'jns_cuti'=>$jns_cuti,
        't_mula' =>$tarikhmula ,
        't_akhir'=> $tarikhakhir,
        'staff_id'=> $staff_id,
        'catatan'=> $sebab_cuti
        

         );

         /*echo "<pre>";
         print_r($datacuti);
         echo "</pre>";
         die();*/

         $success=$this->m_staff->add_staff_data($datacuti);
           if($success==1){

        $d['role']=$this->session->userdata('role');

         $d['title']='test';
         $data['content']=$this->load->view('staff/v_status',$d,true);
         $this->load->view('template/dashboard',$data);

         }else
         {
         echo "Tidak Berjaya";
         }
    }
}
    


/**********************************************************************************************************************************
*End of function add_cuti
**********************************************************************************************************************************/
public function jenis_cuti(){

    $d['role']=$this->session->userdata('role');

    $d['title']='test';
    $data['jenis_cuti']=$this->m_addstaffadmin->getJenisCuti();
    $j['jen_cuti']=$this->load->view('staff/v_permohonan',$data,TRUE);
    $this->load->view('template/dashboard',$j,TRUE);
}

}
    