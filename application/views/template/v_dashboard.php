
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Permohonan Cuti Amtis</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?=  base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    	 <link href="<?=  base_url('assets/css/bg2.css')?>" rel="stylesheet">
       <link href="<?=  base_url('assets/css/dashboard.css')?>" rel="stylesheet">
        <link href="<?=  base_url('assets/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>



  <script language="javascript">
  // Flooble.com's Animated Text script. Will animate a specified 
  // bit of text (determined by the ID of containing tag) by 
  // highlighting it with specified color one character at a time 
  // in a moving pattern.
  //
  // Summary of use: 
  //     call animate(tagID, color); where "tagID" is the ID 
  //     of the tag that contains text to be animated,
  //     and "color" is the color to use to highlight the text with.
  //
  // For more information, and detailed instructions, see 
  //     http://www.flooble.com/scripts/animate.php
  //
  // Copyright (c) 2002 by Animus Pactum Consulting Inc.
  // This script comes with no warranties whatsoever. 
  // Animus Pactum Consulting will not be responsible
  // for any damages resulting from its use.

        var ie4 = false;
        if(document.all) {
                ie4 = true; 
        }       
        function setContent(name, value) {
                var d;  
                if (ie4) { 
                        d = document.all[name];
                } else {
                        d = document.getElementById(name);
                }       
                d.innerHTML = value;    
        }       

  function getContent(name) {
    var d;
                if (ie4) {
                        d = document.all[name];
                } else {
                        d = document.getElementById(name);
                }
                return d.innerHTML;
  }

        function setColor(name, value) {
                var d;  
                if (ie4) { 
                        d = document.all[name];
                } else {
                        d = document.getElementById(name);
                }
                d.style.color = value;  
        }

  function getColor(name) {
                var d;
                if (ie4) {
                        d = document.all[name];
                } else {
                        d = document.getElementById(name);
                }
                return d.style.color;
        }

        function animate(name, col) {
    var value = getContent(name);
    if (value.indexOf('<span') >= 0) { return; }
    var length = 0;
                var str = '';
    var ch;
    var token = '';
    var htmltag = false;  
                for (i = 0; i < value.length; i++) {
      ch = value.substring(i, i+1);
      if (i < value.length - 1) { nextch = value.substring(i+1, i+2); } else { nextch = ' '; }
      token += ch;
      if (ch == '<' && '/aAbBpPhHiIoOuUlLtT'.indexOf(nextch) >= 0) { htmltag = true; }
      if (ch == '>' && htmltag) { htmltag = false; }
      if (!htmltag && ch.charCodeAt(0) > 30 && ch != ' ' && ch != '\n') {   
                          str += '<span id="' + name + '_' + length + '">' + token + '</span>';
        token = '';
        length++;
      }
                }
                setContent(name, str);
                command = 'animateloop(\'' + name + '\', ' + length + ', 0, 1, \'' + col + '\')';
                setTimeout(command , 100);
        }

        function animateloop(name, length, ind, delta, col) {
    var next = ind + delta;
    if (next >= length) { delta = delta * -1; next = ind + delta; }
    if (next < 0) { delta = delta * -1; next = ind + delta; }
                setColor(name + '_' + ind, getColor(name + '_' + next));
                setColor(name + '_' + next, col);
                command = 'animateloop(\'' + name + '\', ' + length + ', ' + next + ', ' + delta + ', \'' + col + '\')';
                setTimeout(command , 100);
        }
</script>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<font face="segoe script" color="yellow"><marquee behavior="alternate" data-target=".bs-example-modal-sm" scrollamount="8">Amtis Management Page</marquee></font>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=site_url('management/welcomepage')?>"<SPAN ID="animate"><b>Pengurusan Cuti</b></SPAN></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="<?=site_url('management/login')?>">Sign Out</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="<?=site_url('management/profil')?>">Profile<span class="sr-only">(current)</span></a></li>
            <li class="active"><a href="<?=site_url('management/semak')?>">Pengurusan Cuti</a></li>
            <li class="active"><a href="<?=site_url('management/staff')?>">Senarai Staff</a></li>
            <li class="active"><a href="<?=site_url('management/tambah')?>">Penambahan Staff</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="<?=site_url('management/update')?>">Kemaskini Profile</a></li>
            <li><a href="<?=site_url('management/tolak')?>">Catatan Tidak Diluluskan</a></li>
            <li><a href="<?=site_url('management/ccuti')?>">Catatan Cuti</a></li>
            <li><a href=""></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="<?=site_url('management/info')?>">Info</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"></h1>

          <?=  $content  ?>

          
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   
    <script src="<?= base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?= base_url('assets/js/moment.min.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>

    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    <SCRIPT language="JavaScript">
   animate('animate', '#00FF00');
    </SCRIPT>

  </body>
</html>

