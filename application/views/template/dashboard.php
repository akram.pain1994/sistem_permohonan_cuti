<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sistem Permohonan Cuti</title>
     <!-- JQUERY-->
    <script src="<?=base_url('assets/js/jquery-1.10.2.js')?>"></script>
    <script src="<?=base_url('assets/js/moment.min.js')?>"> </script>
    <script src="<?=base_url('assets/js/bootstrap-datetimepicker.min.js')?>"> </script>


    <!--BOOTSTRAP DATE.TIME.PICKER-->
     <link href="<?=base_url('assets/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet" />
    <!-- BOOTSTRAP STYLES-->
    <link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="<?=base_url('assets/css/font-awesome.css')?>" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href="<http://fonts.googleapis.com/css?family=Open+Sans" rel='stylesheet' type='text/css' />
   <link href="<?=base_url('assets/css/icon1.css')?>" rel="stylesheet"/>
   <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/bgcontent.css')?>"/>

</head>
<body>


<div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
       <a class="navbar-brand"><marquee>Sistem Permohonan Cuti</marquee></a>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="http://localhost/sistem_permohonan_cuti/" class="btn btn-primary square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                       <?php 
            if($role== 4)
            {
          ?>
                  <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="<?=base_url('assets/img/staff.png')?>" class="user-image img-responsive"/>
                    </li>                   
                    <li>
                        <a  href="http://localhost/sistem_permohonan_cuti/index.php/staff/profil"><i class="glyphicon glyphicon-user round round-lg hollow blue"></i> Profil</a>
                    </li>
                      <li>
                        <a  href="http://localhost/sistem_permohonan_cuti/index.php/staff/permohonan"><i class="glyphicon glyphicon-calendar round round-lg hollow blue"></i> Permohonan Cuti</a>                    
                    <li>
                        <a  href="http://localhost/sistem_permohonan_cuti/index.php/staff/semakan_permohonan"><i class="glyphicon glyphicon-list alt round round-lg hollow blue "></i> Semakan Permohonan</a>
                    </li>
                           <li  >
                        <a  href="#"><i class="#"></i> </a>
                    </li>   
                      <li  >
                        <a  href="#"><i class="#"></i> </a>
                    </li>
                    <li  >
                        <a class="#"  href="#"><i class="#"></i> </a>
                    </li>               
                                          
                            </li>
                        </ul>
                      </li>  
                  <li  >
                        <a   href="#"><i class="#"></i> </a>
                    </li>   
                </ul>
               
            </div>
            <?php
          }
        
            else if($role== 1)
              
            {
          ?>
           <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="<?=base_url('assets/img/admin.png')?>" class="user-image img-responsive"/>
                    </li>                   
                    <li>
                        <a href="http://localhost/sistem_permohonan_cuti/index.php/admin/profil"><i class="glyphicon glyphicon-user round round-lg hollow blue"></i> Profil</a>
                    </li>
                      <li>
                        <a  href="http://localhost/sistem_permohonan_cuti/index.php/admin/pengurusan"><i class="glyphicon glyphicon-calendar round round-lg hollow blue"></i> Pengurusan Cuti</a>                    
                    <li>
                        <a  href="http://localhost/sistem_permohonan_cuti/index.php/admin/tambah"><i class="glyphicon glyphicon-plus round round-lg hollow blue"></i> Penambahan Staff</a>
                    </li>
                           <li  >
                        <a  href="http://localhost/sistem_permohonan_cuti/index.php/admin/update"><i class="glyphicon glyphicon-edit round round-lg hollow blue"></i> Kemaskini Profil</a>
                    </li>   
                      <li  >
                        <a  href="#"><i class="glyphicon glyphicon-file round round-lg hollow blue"></i> Info</a>
                    </li>
                   <li  >
                        <a  href="#"><i class="#"></i> </a>
                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                      </li>  
                    
                </ul>
               
            </div>
            <?php
          }
          ?>
          </div>
      </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12" href="<?=base_url('assets/css/img/bgcontent.css')?>" rel="stylesheet"/>   
                       <?=  $content  ?>
                    </div>
                </div>                      
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>

    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="<?=base_url('assets/js/jquery.metisMenu.js')?>"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="<?=base_url('assets/js/custom.js')?>"></script>
</body>
</html>





































