<legend>Info Cuti</legend>
<b>Senarai Cuti Umum Bagi Tahun 2015</b>
<link href="<?=base_url('assets/css/info.css')?>" rel="stylesheet">
	<div class="row">
     <br><br>
		<div class="error-notice">
          <table class="table table-striped">
            <strong>05-07-2015 / Ahad</strong> - HARI KELEPASAN AM NEGERI
          </div>
          <table class="table table-striped">
            <strong>17-07-2015 / Jumaat</strong> - HARI RAYA PUASA
          </div>
          <table class="table table-striped">
            <strong>18-07-2015 / Sabtu</strong> - HARI RAYA PUASA
          </div>
          <table class="table table-striped">
            <strong>19-07-2015 / Ahad </strong> - HARI KELEPASAN AM NEGERI
          </div>
            <table class="table table-striped">
            <strong>31-08-2015 / Isnin </strong> - HARI KEBANGSAAN
          </div>
          <table class="table table-striped">
            <strong>16-09-2015 / Rabu  </strong> - HARI MALAYSIA
          </div>
          <table class="table table-striped">
            <strong>24-09-2015 / Khamis  </strong> - HARI RAYA HAJI
          </div>
          <table class="table table-striped">
            <strong>25-09-2015 / Jumaat </strong> - HARI RAYA HAJI
          </div>
          <table class="table table-striped">
            <strong>14-10-2015 / Rabu </strong> - AWAL MUHARAM
          </div>
          <table class="table table-striped">
            <strong>10-11-2015 / Selasa   </strong> - HARI DEEPAVALI
          </div>
          <table class="table table-striped">
            <strong>24-12-2015 / Khamis  </strong> - HARI KEPUTERAAN NABI MUHAMMAD S.A.W
          </div>
          <table class="table table-striped">
            <strong>25-12-2015 / Jumaat </strong> - HARI KRISMAS
          </div>
	</div>
</div>
