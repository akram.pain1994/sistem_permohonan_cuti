<script type="text/javascript">
            $(function () { 
                $('#tarikhmula').datepicker();
                $('#tarikhakhir').datepicker(); 
                
                 
            });
        </script>
<form class="form-horizontal" action='<?=site_url('staff/add_cuti')?>' method='post'>
<fieldset>

<!-- Form Name -->
<legend>Permohonan Cuti</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nama * :</label>  
  <div class="col-md-4">
 <input id="textinput" value='<?= $staff_id[0]->nama ?>'name="Nama" type="text" placeholder="Nama" class="form-control input-md">
   
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Staff ID * :</label>  
  <div class="col-md-4">
  <input id="textinput" value='<?= $staff_id[0]->staff_id ?>' name="StaffID" type="text" placeholder="Staff ID" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">E-mail * :</label>  
  <div class="col-md-4">
  <input id="textinput" value='<?= $staff_id[0]->email ?>' name="Email" type="text" placeholder="abc@acb.com.my" class="form-control input-md">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Tarikh Mula * :</label>  
  <div class="col-md-4">
  <input id="tarikhmula" name="tarikhmula" type="text"  class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Tarikh Akhir * :</label>  
  <div class="col-md-4">
  <input id="tarikhakhir" name="tarikhakhir" type="text"  class="form-control input-md">
    
  </div>
</div>
<!-- hidden input -->
<input type='hidden' name='staff_id' value='<?= $staff[0]->no_id ?>'>
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Jenis Cuti * :</label>  
  <div class="col-md-4">
<select class="form-control" name="Jenis_Cuti">
<option>-- Sila Pilih--</option>
  <?php
  foreach($jenis_cuti as $row)
  {
    ?>

    <option value="<?=$row->jen_cuti?>"><?=$row->applied_for?></option>

  <?php  
  }
  ?>
</select>
  </div>
</div>
<!-- Multiple Radios -->
<!-- <div class="form-group">
  <label class="col-md-4 control-label" for="jns_cuti">Jenis Cuti * :</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="jns_cuti-0">
      <input type="radio" name="jns_cuti" id="jns_cuti-0" value="1" checked="checked">
      Sakit
    </label>
  </div>
  <div class="radio">
    <label for="jns_cuti-1">
      <input type="radio" name="jns_cuti" id="jns_cuti-1" value="2" checked="checked">

      Berbayar
    </label>
  </div>
  <div class="radio">
    <label for="jns_cuti-2">
      <input type="radio" name="jns_cuti" id="jns_cuti-2" value="3" checked="checked">
      Tidak Berbayar
    </label>
  </div>
  <div class="radio">
    <label for="jns_cuti-3">
      <input type="radio" name="jns_cuti" id="jns_cuti-3" value="4" checked="checked"> 
      Kecemasan
    </label>
  </div>
  <div class="radio">
    <label for="jns_cuti-4">
      <input type="radio" name="jns_cuti" id="jns_cuti-4" value="5" checked="checked">
      Lain-lain
    </label>
  </div>
  </div> -->


<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">Sebab-sebab * :</label>
  <div class="col-md-4">                    
     <input id="sebab_cuti" name="sebab cuti" type="text"  class="form-control input-md">
 </div>
</div>
    <!-- hidden input -->
<input type='hidden' name='staff_id' value=''>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <input type='submit' value='Submit'  name='add_cuti' class="btn btn-primary btn-primary"/>

  </div>
</div>


</fieldset>
</form>
