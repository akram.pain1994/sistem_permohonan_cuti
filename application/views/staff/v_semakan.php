
<fieldset>

<!-- Form Name -->
<legend>Semakan Permohonan Cuti</legend>

<div class="container">
	<div class="row">
        <div class="col-md-4">
            <form action="#" method="get">
                <div class="input-group">
                    </span>
                </div>
            </form>
        </div>
		<div class="col-md-10">
    	 <table class="table table-list-search">
                <thead> 
                        <tr>
                            <th></th>
                            <th>Staff ID</th>
                            <th>Jenis Cuti</th>
                            <th>Tempoh Cuti</th>
                            <th>Sebab</th>
                            <th>Pengesahan</th>            
                        </tr>
                </thead>
                <tbody>

                       <?php if ($semakan){
                       foreach ($semakan as $value) {?>
                        <tr>
                            <td><p>&#10145;<p></td>
                            <td><?=$value->staff_id;  ?></td>
                            <td><?=$value->applied_for;  ?></td>
                            <td><?=$value->t_mula;  ?> - <?=$value->t_akhir;  ?></td>
                            <td><?=$value->catatan;  ?></td>
                            <td><?=$value->kelulusan;  ?></td>
                            <td></td>                           
                        </tr>
                    <?php
          }
}?>                  
                
                    </tbody>
                </table>   
		</div>
	</div>
</div>