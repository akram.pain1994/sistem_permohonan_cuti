<h1 class="site__title mega">Info Cuti</h1>

<b>Senarai Cuti Umum Bagi Tahun 2015</b>
<link href="<?=  base_url('assets/css/info.css')?>" rel="stylesheet">
<link href="<?=  base_url('assets/css/animated.css')?>" rel="stylesheet">
	<div class="row">
     <br><br>
     <table>
		<div class="error-notice">
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>05-07-2015 / Ahad</strong><font color="Red"> - HARI KELEPASAN AM NEGERI</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>17-07-2015 / Jumaat</strong><font color="Red"> - HARI RAYA PUASA</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>18-07-2015 / Sabtu</strong><font color="Red"> - HARI RAYA PUASA</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>19-07-2015 / Ahad </strong><font color="Red"> - HARI KELEPASAN AM NEGERI</font>
          </div>
            <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>31-08-2015 / Isnin </strong><font color="Red"> - HARI KEBANGSAAN</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>16-09-2015 / Rabu  </strong><font color="Red"> - HARI MALAYSIA</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>24-09-2015 / Khamis  </strong><font color="Red"> - HARI RAYA HAJI</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>25-09-2015 / Jumaat </strong><font color="Red"> - HARI RAYA HAJI</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>14-10-2015 / Rabu </strong><font color="Red"> - AWAL MUHARAM</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>10-11-2015 / Selasa   </strong><font color="Red"> - HARI DEEPAVALI</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>24-12-2015 / Khamis  </strong><font color="Red"> - HARI KEPUTERAAN NABI MUHAMMAD S.A.W</font>
          </div>
          <div class="large" style="border: black 1px dashed; border-bottom: black 1px solid; border-top-style: ridge;">
            <strong>25-12-2015 / Jumaat </strong><font color="Red"> - HARI KRISMAS</font>
          </div>

      </table>
	</div>
</div>
