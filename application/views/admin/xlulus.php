
<link href="<?=  base_url('assets/css/table.css')?>" rel="stylesheet">


<div class="span7">   
  <div class="widget stacked widget-table action-table">
                    
                <div class="widget-header">
                    <i class="glyphicon glyphicon-th-list"></i>
                    <h3>Pengurusan Cuti</h3>
                </div> <!-- /widget-header -->
                
                <div class="widget-content">
                    
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="70px">Staff ID</th>
                                <th width="180px">Nama</th>
                                <th width="70px">Jenis Cuti</th>
                                <th width="195px">Tempoh Cuti</th>
                                <th align="center">Sebab</th>
                                <th class="td-actions" width="150px">Kelulusan</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            <?php foreach($posts as $post){ ?>
                            <tr>
                                <td><?php echo $post->no_id;?></td>
                                <td><?php echo $post->username;?></td>
                                <td><?php echo $post->jnis_cuti;?></td>
                                <td><?php echo $post->cuti_mula;?>-
                                    <?php echo $post->cuti_akhir;?></td>
                                <td><?php echo $post->catatan;?></td>
                                <td><?php echo $post->cuti_taklulus;?></td>
                            </tr>

                            <?php } ?>
                              <!--
                              <tr>
                                <td>Trident</td>
                                <td>Username</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                <td class="td-actions">
                                    <a href="#" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-ok-circle"></span> Lulus</a>
                                        <i class="btn-icon-only icon-ok"></i>                                       
                                    </a>
                                    
                                    <a href="#" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove-circle"></span> Tidak</a>
                                        <i class="btn-icon-only icon-remove"></i>                                       
                                    </a>
                                </td>
                            </tr>
                              <tr>
                                <td>Trident</td>
                                <td>Username</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                <td class="td-actions">
                                    <a href="#" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-ok-circle"></span> Lulus</a>
                                        <i class="btn-icon-only icon-ok"></i>                                       
                                    </a>
                                    
                                    <a href="#" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove-circle"></span> Tidak</a>
                                        <i class="btn-icon-only icon-remove"></i>                                       
                                    </a>
                                </td>
                            </tr>
                              <tr>
                                <td>Trident</td>
                                <td>Username</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                <td class="td-actions">
                                    <a href="#" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-ok-circle"></span> Lulus</a>
                                        <i class="btn-icon-only icon-ok"></i>                                       
                                    </a>
                                    
                                    <a href="#" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove-circle"></span> Tidak</a>
                                        <i class="btn-icon-only icon-remove"></i>                                       
                                    </a>
                                </td>
                            </tr>
                             <tr>
                                <td>Trident</td>
                                <td>Username</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                <td class="td-actions">
                                    <a href="#" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-ok-circle"></span> Lulus</a>
                                        <i class="btn-icon-only icon-ok"></i>                                       
                                    </a>
                                    
                                    <a href="#" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove-circle"></span> Tidak</a>
                                        <i class="btn-icon-only icon-remove"></i>                                       
                                    </a>
                                </td>
                            </tr>
                             <tr>
                                <td>Trident</td>
                                <td>Username</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                     <td>Trident</td>
                                <td class="td-actions">
                                    <a href="#" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-ok-circle"></span> Lulus</a>
                                        <i class="btn-icon-only icon-ok"></i>                                       
                                    </a>
                                    
                                    <a href="#" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove-circle"></span> Tidak</a>
                                        <i class="btn-icon-only icon-remove"></i>                                       
                                    </a>
                                </td>
                            </tr>-->
                            </tbody>
                        </table>
                </div>
            
            </div>
            </div>