

 <?php echo validation_errors();?>
<form class="form-horizontal" action='<?=site_url('admin/tambah')?>' method='post'>
<fieldset>
 
<!-- Form Name -->
<div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"> TAMBAH STAFF</h3>
            </div></div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nama* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Nama" type="text" placeholder="Nama" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Alamat* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Alamat" type="text" placeholder="Warganegara" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nombor Tel* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="No_Tel" type="text" placeholder="01x-xxxxxxx" class="form-control input-md" required="">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">E-mail* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Email" type="text" placeholder="abc@abc.com.my" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">No ID* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Staff_id" type="text" placeholder="1234" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Role ID* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Role_id" type="text" placeholder="1,2,3,4" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Username* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Username" type="text" placeholder="Contoh" class="form-control input-md" required="">
    
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Password* :</label>  
  <div class="col-md-4">
  <input id="textinput" name="Password" type="text" placeholder="Contoh" class="form-control input-md" required="">
    
  </div>
</div>


<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
    <button id="button1id" name="button1id" class="btn btn-success" value="Insert" type="submit"><span class="glyphicon glyphicon-floppy-open"></span>    ADD   </button>
    <button id="button2id" name="button2id" class="btn btn-danger" type="reset" value="Cancel"><span class="glyphicon glyphicon-floppy-remove"></span>   CLEAR</button>
  </div>

  <?php echo form_close(); ?>
        <?php echo $this->session->flashdata('msg'); ?>
          
</div>

</fieldset>
</form>
