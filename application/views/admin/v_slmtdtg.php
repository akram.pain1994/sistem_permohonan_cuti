<!DOCTYPE html>
<html>
  <head>
  	<!-- <link rel="stylesheet" type="text/css" href="../application/views/admin/custom.css"> -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="http://s3.amazonaws.com/codecademy-content/courses/hour-of-code/js/alphabet.js"></script>
  </head>
  <body>
  	<div>
    	<canvas id="myCanvas" class="canvas1"></canvas>
	</div>
    
    <script type="text/javascript" src="<?= base_url('assets/js/buble.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/main.js')?>"></script>

<center><a href="<?=site_url('admin/profil')?>" class="btn btn-success">Ke Paparan Maklumat  <span class="glyphicon glyphicon-arrow-right"></span></a></center>
  </body>
</html>